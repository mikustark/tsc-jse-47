package ru.tsc.karbainova.tm.exception.empty;

import ru.tsc.karbainova.tm.exception.AbstractException;

public class EmptySessionNlException extends AbstractException {
    public EmptySessionNlException() {
        super("Error Session IS Null.");
    }

    public EmptySessionNlException(String value) {
        super("Error Session IS Null. " + value);
    }
}
